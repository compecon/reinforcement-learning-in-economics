# Dynamic Programming

Bandits help us to gain a better understanding of action, action-values and update rules.
Furthermore, the exploration-exploitation trade-off can be illustrated with a 
k-armed bandit.


## Learning Goals

* Understand value functions
* Understand Bellman equations
* Understand General Policy Iteration (GPI)
* Solve problems with Policy Iteration and Value Iteration


## Summary

* Dynamic Programming requires a full model of the MDP
* GPI it an iteratively process doing policy evaluation and improvement


## Lectures

* [Lecture Slides](https://www.overleaf.com/read/ctkdprhttrbm)

## Exercises

* Work through the [DP Notebook](https://colab.research.google.com/drive/1dskCDJSmDNJrYzKbnyId259hRKhQcv-P)

