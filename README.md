# Reinforcement-Learning in Economics

This repository contains material for the seminar "Reinforcement Learning in Economics" which will be taught at RWTH Aachen university. 
This course is based on the textbook [Reinforcement Learning: An Introduction](http://incompleteideas.net/book/RLbook2018.pdf) from Richard S. Sutton and Andrew G. Barto. 

You find codes and exercises in this repository. Lecture slides and solutions are provided via [Moodle](https://moodle.rwth-aachen.de/). All code is written in Python3 and uses the RL framework [OpenAI Gym](https://gym.openai.com/).

Jupyter notebooks can be run with [Google Colabs](https://colab.research.google.com) if you have a Google account. You can also run them on your local machine, but then you have to installed all necessary packages by yourself.

## Learning Goals

* Understand Markov Decision Processes and their dynamics
* Learn to compute policy and value functions
* Understand how to formalize your task as a RL problem
* Implement a full RL solution to solve economic problems

## Table of contents

* [Prerequisites](https://git.rwth-aachen.de/compecon/reinforcement-learning-in-economics/-/tree/master/0-Prerequisite)
* [Introduction](https://git.rwth-aachen.de/compecon/reinforcement-learning-in-economics/-/tree/master/1-Introduction)
* [Bandits](https://git.rwth-aachen.de/compecon/reinforcement-learning-in-economics/-/tree/master/2-Bandits)
* [Dynamic Programming](https://git.rwth-aachen.de/compecon/reinforcement-learning-in-economics/-/tree/master/3-Dynamic_Programming)
* Monte-Carlo
* Temporal Difference
* Approximation
* Policy Gradient



Reinforcement Learning is a subfield of Machine Learning which is suitable for 
sequential decision making. In this course you will learn about the basic
RL framework and how to model a problem as a Markov Decision Process. You will 
learn how to solve those problems with different algorithms using Python and OpenAI Gym.
In particular, you will study Dynamic Programming, Monte-Carlo Methods and Temporal
Difference. First you will solve small, discrete problems and later more complex 
problems using function approximation like neural networks. At the end of the course, you
are expected to solve an economic problem on your own in teams of two or
three. We will be able to provide possible topics, but students are highly
encouraged to come up with their own ideas and topics for their final project.

