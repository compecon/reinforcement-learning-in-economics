# Introduction 

## Learning goals

* Understand the Reinforcement Learning problem
* Understand the vocabulary
* Understand the basic structure of OpenAI Gym

## Summary

* Reinforcement Learning tries to find an optimal policy for sequential decision
making. 
* It is well suited for optimal control problems in discrete time.
* An agent learns from experience by interacting with the environment.
* The environment is defined by its states, the state transition dynamics and 
the reward function


## Lectures

* [Lecture Slides](https://www.overleaf.com/read/ctkdprhttrbm)


## Exercises

* Work through the [Introduction Notebook](https://colab.research.google.com/drive/12CmUjUt2WrEtp1pYsCIeRTlPSQ1zg27D?usp=sharing)