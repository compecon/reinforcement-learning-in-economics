# Bandits

Bandits help us to gain a better understanding of action, action-values and update rules.
Furthermore, the exploration-exploitation trade-off can be illustrated with a 
k-armed bandit.


## Learning Goals

* Understand action and action-values
* Understand action-value estimation methods 
* Define exploration and exploitation
* Create a custom environment


## Summary

* Bandits contain no states


## Lectures

* [Lecture Slides](https://www.overleaf.com/read/ctkdprhttrbm)

## Exercises

* Work through the [Bandit Notebook](https://colab.research.google.com/drive/1xGp0eifETpkDewYjI1U19r3GmgdgYOPL)

