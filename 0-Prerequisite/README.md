# Prerequisite

Students should be comfortable with probabilities & expectations, basic linear algebra, basic calculus and Python.

## Learning goals

* Refresh your Python skills

## Resources

* [Python like you mean it](https://www.pythonlikeyoumeanit.com/) is a great resource to refresh your Pyhton skills
* Let me know if you know of any other good resources, I will include them here

## Exercises

The notebook [Prerequisites](https://colab.research.google.com/drive/13bA8Y8a6Jz0qXZp9byImWH8W9iIukNfi?usp=sharing) privides some exercises which may be useful for this course.